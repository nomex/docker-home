# Global
DOMAIN: ${nomex/global/domain}
DOCKER_PATH: ${nomex/global/docker_path}
DOWNLOADS_PATH: ${nomex/global/downloads_path}
DATA_PATH: ${nomex/global/data_path}
BACKUP_PATH: ${nomex/global/backup_path}

# Transmission
TRANSMISSION_USER: ${nomex/transmission/user}
TRANSMISSION_PASS: ${nomex/transmission/password}

# Traefik and CloudFlare-ddns
CF_API_EMAIL: ${nomex/traefik/cf_email}
CF_DNS_API_TOKEN: ${nomex/traefik/cf_api_token}
CF_ZONE_API_TOKEN: ${nomex/traefik/cf_api_token}

# Plex
PLEX_CLAIM: ${nomex/plex/plex_claim}
TVSHOWS_PATH: ${nomex/plex/tvshows_path}
MOVIES_PATH: ${nomex/plex/movies_path}
BOAT_PATH: ${nomex/plex/boat_path}
MMC_PATH: ${nomex/plex/mmc_path}
ING_PATH: ${nomex/plex/ing_path}
PADRES_PATH: ${nomex/plex/padres_path}
DRONES_PATH: ${nomex/plex/drones_path}
TRAINING_PATH: ${nomex/plex/training_path}
FITNESS_PATH: ${nomex/plex/fitness_path}
F1_PATH: ${nomex/plex/f1_path}
PLEX_LOGS_PATH: ${nomex/plex/plex_logs_path}
PLEX_ADVERTISE_IP: ${nomex/plex/plex_advertise_ip}

# Flexget
FG_WEBUI_PASSWD: ${nomex/flexget/fg_webui_passwd}

# Watchtower
WATCHTOWER_NOTIFICATION_EMAIL_FROM: ${nomex/watchtower/from}
WATCHTOWER_NOTIFICATION_EMAIL_TO: ${nomex/watchtower/to}
WATCHTOWER_NOTIFICATION_EMAIL_SERVER: ${nomex/watchtower/server}
WATCHTOWER_NOTIFICATION_EMAIL_SERVER_USER: ${nomex/watchtower/server_user}
WATCHTOWER_NOTIFICATION_EMAIL_SERVER_PASSWORD: ${nomex/watchtower/server_password}

# Samba
SAMBAUSER: ${nomex/samba/sambauser}
SAMBASHARE: ${nomex/samba/sambashare}
WORKGROUP: ${nomex/samba/workgroup}

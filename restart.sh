#!/usr/bin/env bash

# Run all the docker
secrethub run --template secrets.env.tpl -- docker-compose down
secrethub run --template secrets.env.tpl -- docker-compose up -d


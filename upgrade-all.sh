#!/usr/bin/env bash

# Stopping all docker
secrethub run --template secrets.env.tpl -- docker-compose stop

# Pulling latest images
secrethub run --template secrets.env.tpl -- docker-compose pull

# Run all the docker
secrethub run --template secrets.env.tpl -- docker-compose up -d

# Delete dangling images
docker image prune -f

